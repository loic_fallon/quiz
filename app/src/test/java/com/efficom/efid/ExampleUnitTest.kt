package com.efficom.efid

import com.efficom.efid.data.model.*
import org.junit.Test

import org.junit.Assert.*

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {

    val room = Room(id_salle = 12)
    @Test
    fun addition_isCorrect() {
        assertEquals(4, 2 + 2)
    }

    @Test
    fun isGoodRoom(){
        assertEquals(12, room.id_salle)
    }
}
